package de.e2.ktor.session

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.sessions.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@Serializable
data class MySessionExt(
    val counter: Int,
    @Serializable(with = LocalDateTimeSerializer::class) val startDate: LocalDateTime = LocalDateTime.now()
)

@Serializer(forClass = LocalDateTime::class)
object LocalDateTimeSerializer : KSerializer<LocalDateTime> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("LocalDateTime", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: LocalDateTime) {
        encoder.encodeString(value.format(DateTimeFormatter.ISO_DATE_TIME))
    }

    override fun deserialize(decoder: Decoder): LocalDateTime {
        return LocalDateTime.parse(decoder.decodeString(), DateTimeFormatter.ISO_DATE_TIME)
    }
}


class MySessionSerializer : SessionSerializer<MySessionExt> {
    override fun deserialize(text: String) = Json.decodeFromString(MySessionExt.serializer(), text)

    override fun serialize(session: MySessionExt) = Json.encodeToString(
        MySessionExt.serializer(), session
    )

}

fun Application.sessionExt() {
    install(Sessions) {
        cookie<MySessionExt>("COOKIE_NAME") {
            serializer = MySessionSerializer()
        }

    }
    routing {
        get("/session") {
            var mySession = call.sessions.get<MySessionExt>()
            if (mySession == null) {
                mySession = MySessionExt(1)
            } else {
                mySession = mySession.copy(counter = mySession.counter + 1)
            }

            call.sessions.set(mySession)
            call.respondText("Session: $mySession")
        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::sessionExt)
    println("Open with: http://localhost:8080/session")
    server.start(wait = true)
}


